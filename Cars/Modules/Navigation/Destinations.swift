//
//  Destinations.swift
//  Cars
//
//  Created by Eslam Mostafa on 15/02/2022.
//

import UIKit

enum Destinations {
    case carMakesVC
    case carModelsVC(carMakeName: String)
    case carPhotosVC(carMake: String, carModel: String, category: String)
    case detailsVC(carMake: String, modelNiceName: String, modelName: String)
    
    var viewcontroller: UIViewController {
        switch self {
        case .carMakesVC:
            let viewModel = CarMakesViewModel()
            return CarMakesViewController(with: viewModel)
        case .carModelsVC(let carMakeName):
            let viewModel = CarModelsViewModel(carMakeName: carMakeName)
            return CarModelsViewController(with: viewModel)
        case .carPhotosVC(carMake: let carMake, carModel: let carModel, category: let category):
            let viewModel = CarPhotosViewModel(carMake: carMake, carModel: carModel, category: category)
            let vc = CarPhotosViewController(with: viewModel)
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            return vc
        case .detailsVC(let carMake, let modelNiceName, let modelName):
            let viewModel = DetailsViewModel(makeName: carMake, modelNiceName: modelNiceName , modelName: modelName)
            return DetailsViewController(with: viewModel)
        }
    }
}
