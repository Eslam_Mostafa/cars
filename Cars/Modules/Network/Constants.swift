//
//  Constants.swift
//  Cars
//
//  Created by Eslam Mostafa on 15/02/2022.
//

import Foundation

struct Constants {
    //The API's base URL
    static let BASEURL = "https://api.edmunds.com/api/vehicle/v3"
    static let IMAGEBASEURL = "https://api.edmunds.com/api/media/v2"
    static let IMAGESOURCE = "https://media.ed.edmunds-media.com/"
    static let APIKEY  = "2ep93tpnyh6p5hgaxmp6pasq"
    static let STATE   = "NEW"
    static let YEAR    = 2022
    static let RESPONSE_FORMAT = "json"
    static let PHOTOWIDTH = "600"
    static let PHOTOHEIGHT = "400"
    static let EXTERIOR = "exterior"
    static let INTERIOR = "interior"
    static let MODELFIELDS = "name,niceName,makeNiceName"
    static let STYLESFIELDS = "engineType,engineSize,numberOfSeats,transmissionType,categories"
    
    //The parameters (Queries) that we're gonna use
    struct Parameters {
        static let APIKEY = "api_key"
        static let PAGE_NUMBER = "pageNum"
        static let PAGE_SIZE = "pageSize"
        static let SORT_BY = "sortby"
        static let RESPONSE_FIELDS = "fields"
        static let STATE   = "publicationStates"
        static let YEAR    = "year"
        static let RESPONSE_FORMAT = "fmt"
        static let MAKENAME = "makeName"
        static let CATEGORY = "category"
        static let MODELNICENAME = "modelNiceName"
        static let MAKENICENAME = "makeNiceName"
        static let PHOTOWIDTH = "width"
        static let PHOTOHEIGHT = "height"
    }
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}
