//
//  CarMakesAPI.swift
//  Cars
//
//  Created by Eslam Mostafa on 15/02/2022.
//

import Foundation
import Alamofire

enum CarMakesAPI: URLRequestConvertible {
    case makes(pageNum: String, pageSize: String, sortby: String, responseFields: String)
}

extension CarMakesAPI: ApiRouter {
    var path: String {
        switch self {
        case .makes:
            return "makes"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .makes:
            return .get
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .makes(let pageNum , let pageSize , let sortby ,let responseFields):
            return [
                Constants.Parameters.APIKEY : Constants.APIKEY,
                Constants.Parameters.PAGE_NUMBER : pageNum,
                Constants.Parameters.PAGE_SIZE : pageSize,
                Constants.Parameters.SORT_BY : sortby,
                Constants.Parameters.RESPONSE_FIELDS : responseFields
            ]
        }
    }
}
