//
//  CarMakesModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 16/02/2022.
//

import Foundation

struct CarMakesModel: Codable {
    let totalNumber: Int?
    let totalPages: Int?
    let results: [CarMakesNamesModel]?
    
    enum CodingKeys: String, CodingKey {
        case totalNumber = "totalNumber"
        case totalPages = "totalPages"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalNumber = try values.decodeIfPresent(Int.self, forKey: .totalNumber)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        results = try values.decodeIfPresent([CarMakesNamesModel].self, forKey: .results)
    }
}

struct CarMakesNamesModel: Codable , Hashable {
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
}
