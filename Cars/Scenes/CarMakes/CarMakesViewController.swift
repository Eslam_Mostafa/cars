//
//  CarMakesViewController.swift
//  Cars
//
//  Created by Eslam Mostafa on 15/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class CarMakesViewController: UIViewController, Loadable {
    
    // MARK: - Outlets
    @IBOutlet private var carMakesCollectionView: UICollectionView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: CarMakesViewModelProtocol
    
    // MARK: - Initialization
    init(with viewModel: CarMakesViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUIText()
        setupCarMakesCollection()
        configureBinding()
        viewModel.getCarMakesApi()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        carMakesCollectionView.reloadData()
    }
    
    // MARK: - Methods
    private func setUIText() {
        self.title = "Car makes"
    }
    
    private func setupCarMakesCollection() {
        carMakesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        carMakesCollectionView.register(CarMakesCollectionViewCell.nib, forCellWithReuseIdentifier: CarMakesCollectionViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.showProgress
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self]  in
                guard let self = self else { return }
                self.showLoading(show: $0)
            }).disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(alertTitle: "Something wrong!", alertMessge: error.localizedDescription, actionTitle: "Cancel")
            }).disposed(by: disposeBag)
        
        viewModel.carMakesListSubject.bind(to: carMakesCollectionView.rx.items(cellIdentifier: CarMakesCollectionViewCell.identifier, cellType: CarMakesCollectionViewCell.self)) { _, carMakesModel, cell in
            cell.setupCell(with: carMakesModel)
        }.disposed(by: disposeBag)
        
        carMakesCollectionView.rx.modelSelected(CarMakesNamesModel.self)
            .subscribe(onNext: { [weak self] model in
                guard let self = self else { return }
                self.viewModel.navigateToCarModelVC(carMakeName: model.name ?? "")
            }).disposed(by: disposeBag)
    }
}

// MARK: - CollectionView Delegate Methods
extension CarMakesViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var numCellInRow : CGFloat = 2
        
        if UIDevice.current.orientation.isLandscape {
            numCellInRow = 3
        }
        
        let width = (collectionView.bounds.width) / numCellInRow
        return CGSize(width: width, height: 64)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.carMakesCollectionView {
            let endScrolling = (scrollView.contentOffset.y + scrollView.frame.size.height)
            
            if endScrolling >= scrollView.contentSize.height {
                if viewModel.checkIsHasNext() {
                    viewModel.getCarMakesApi()
                }
            }
        }
    }
}
