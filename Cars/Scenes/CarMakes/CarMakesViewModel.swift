//
//  CarMakesViewModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 15/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol CarMakesViewModelProtocol {
    var carMakesListSubject: BehaviorRelay<[CarMakesNamesModel]> { get }
    func getCarMakesApi()
    func checkIsHasNext() -> Bool
    func navigateToCarModelVC(carMakeName: String)
    var showProgress: PublishSubject<Bool> { get }
    var error: PublishSubject<Error> { get }
}

class CarMakesViewModel: CarMakesViewModelProtocol {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    let error = PublishSubject<Error>()
    let showProgress = PublishSubject<Bool>()
    private var carMakesApi: ApiClientProtocol
    private var totalPages: Int = 0
    private var pageNumber: Int = 1
    let carMakesListSubject = BehaviorRelay<[CarMakesNamesModel]>(value: [])
    private var carMakesList: [CarMakesNamesModel] = [] {
        didSet {
            self.carMakesListSubject.accept(getSortedDistinctMakes())
        }
    }
    
    // MARK: - Initialization
    init(carMakesApi: ApiClientProtocol = ApiClient()) {
        self.carMakesApi = carMakesApi
    }
    
    // MARK: - Methods
    func checkIsHasNext() -> Bool {
        return (pageNumber > totalPages) ? false : true
    }
    
    func navigateToCarModelVC(carMakeName: String) {
        try? AppNavigator().push(.carModelsVC(carMakeName: carMakeName))
    }
    
    private func getSortedDistinctMakes() -> [CarMakesNamesModel] {
        return Array(Set(carMakesList)).sorted(by: { ($0.name ?? "").lowercased() < ($1.name ?? "").lowercased() })
    }
    
    func getCarMakesApi() {
        showProgress.onNext(true)
        self.carMakesApi.request(CarMakesAPI.makes(pageNum: "\(pageNumber)", pageSize: "30", sortby: "name:ASC", responseFields: "name"), decodingType: CarMakesModel.self)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                self.carMakesList.append(contentsOf: response.results ?? [])
                self.totalPages = response.totalPages ?? 0
                self.pageNumber += 1
                self.showProgress.onNext(false)
            }, onError: { error in
                self.error.onNext(error)
                self.showProgress.onNext(false)
            })
            .disposed(by: disposeBag)
    }
}
