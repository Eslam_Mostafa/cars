//
//  CarMakesCollectionViewCell.swift
//  Cars
//
//  Created by Eslam Mostafa on 16/02/2022.
//

import UIKit

class CarMakesCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var carMakeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - Properties
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    //MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCellUI()
    }
    
    //MARK: - Methods
    func setupCell(with dataModel: CarMakesNamesModel) {
        carMakeLabel.text = dataModel.name
    }
    
    private func setupCellUI() {
        containerView.layer.cornerRadius = 16
        containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#F5F7FB")
    }
}
