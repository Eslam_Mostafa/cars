//
//  CarModelsAPI.swift
//  Cars
//
//  Created by Eslam Mostafa on 17/02/2022.
//

import Foundation
import Alamofire

enum CarModelsAPI: URLRequestConvertible {
    case models(carMakeName: String, pageNum: String, pageSize: String, responseFields: String)
}

extension CarModelsAPI: ApiRouter {
    var path: String {
        switch self {
        case .models:
            return "models"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .models:
            return .get
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .models(let carMakeName ,let pageNum , let pageSize ,let responseFields):
            return [
                Constants.Parameters.MAKENAME : carMakeName,
                Constants.Parameters.APIKEY : Constants.APIKEY,
                Constants.Parameters.RESPONSE_FORMAT : Constants.RESPONSE_FORMAT,
                Constants.Parameters.STATE : Constants.STATE,
                Constants.Parameters.YEAR : Constants.YEAR,
                Constants.Parameters.PAGE_NUMBER : pageNum,
                Constants.Parameters.PAGE_SIZE : pageSize,
                Constants.Parameters.RESPONSE_FIELDS : responseFields
            ]
        }
    }
}

enum CarPhotosAPI: URLRequestConvertible {
    case photos(carMakeName: String, carModelName: String, year: String, category: String)
}

extension CarPhotosAPI: ApiRouter {
    var path: String {
        switch self {
        case .photos(let carMakeName, let carModelName, let year, _):
            return "\(carMakeName)/\(carModelName)/\(year)/photos"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .photos:
            return .get
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .photos(_ , _ , _ , let category):
            return [
                Constants.Parameters.APIKEY : Constants.APIKEY,
                Constants.Parameters.RESPONSE_FORMAT : Constants.RESPONSE_FORMAT,
                Constants.Parameters.CATEGORY : category,
                Constants.Parameters.PHOTOWIDTH : Constants.PHOTOWIDTH,
                Constants.Parameters.PHOTOHEIGHT : Constants.PHOTOHEIGHT
            ]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try (Constants.IMAGEBASEURL).asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        //Http method
        urlRequest.httpMethod = method.rawValue
        // Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}
