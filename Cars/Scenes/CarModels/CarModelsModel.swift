//
//  CarModelsModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 17/02/2022.
//

import Foundation

struct CarModelsModel: Codable {
    let totalNumber: Int?
    let totalPages: Int?
    let results: [CarModelsNames]?
    
    enum CodingKeys: String, CodingKey {
        case totalNumber = "totalNumber"
        case totalPages = "totalPages"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalNumber = try values.decodeIfPresent(Int.self, forKey: .totalNumber)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        results = try values.decodeIfPresent([CarModelsNames].self, forKey: .results)
    }
}

struct CarModelsNames: Codable {
    let name: String?
    let niceName: String?
    let makeNiceName: String?
    var imageSource = ""
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case niceName = "niceName"
        case makeNiceName = "makeNiceName"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        niceName = try values.decodeIfPresent(String.self, forKey: .niceName)
        makeNiceName = try values.decodeIfPresent(String.self, forKey: .makeNiceName)
    }
}
