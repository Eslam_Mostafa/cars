//
//  CarModelsViewController.swift
//  Cars
//
//  Created by Eslam Mostafa on 17/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class CarModelsViewController: UIViewController, Loadable {
    
    // MARK: - Outlets
    @IBOutlet private var carModelsCollectionView: UICollectionView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: CarModelsViewModelProtocol
    
    // MARK: - Initialization
    init(with viewModel: CarModelsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUIText()
        setupCarModelsCollection()
        configureBinding()
        viewModel.getCarModelLApi()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        carModelsCollectionView.reloadData()
    }
    
    // MARK: - Methods
    private func setUIText() {
        self.title = "Models"
    }
    
    private func setupCarModelsCollection() {
        carModelsCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        carModelsCollectionView.register(CarModelsCollectionViewCell.nib, forCellWithReuseIdentifier: CarModelsCollectionViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.showProgress
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self]  in
                guard let self = self else { return }
                self.showLoading(show: $0)
            }).disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(alertTitle: "Something wrong!", alertMessge: error.localizedDescription, actionTitle: "Cancel")
            }).disposed(by: disposeBag)
        
        viewModel.carModelListSubject.skip(1)
            .subscribe(onNext: { [weak self] list in
                if list.count == 0 {
                    self?.showAlertWithBackAction()
                }
            }).disposed(by: disposeBag)
        
        viewModel.carModelListSubject.bind(to: carModelsCollectionView.rx.items(cellIdentifier: CarModelsCollectionViewCell.identifier, cellType: CarModelsCollectionViewCell.self)) { index, carMakesModel, cell in
            if carMakesModel.imageSource.isEmpty {
                self.viewModel.getCarPhotosApi(modelName: carMakesModel.name ?? "", index: index)
            }
            cell.setupCell(with: carMakesModel)
        }.disposed(by: disposeBag)
        
        carModelsCollectionView.rx.modelSelected(CarModelsNames.self)
            .subscribe(onNext: { [weak self] model in
                guard let self = self else { return }
                self.viewModel.navigateToDetailsVC(modelNiceName: model.niceName ?? "", modelName: model.name ?? "")
            }).disposed(by: disposeBag)
    }
    
    private func showAlertWithBackAction() {
        let alertController = UIAlertController(title: "Error", message: "No models available", preferredStyle: .alert)
        let oKAction = UIAlertAction(title: "OK", style: .default) { action in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(oKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - CollectionView Delegate Methods
extension CarModelsViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var numCellInRow : CGFloat = 1
        
        if UIDevice.current.orientation.isLandscape {
            numCellInRow = 2
        }
        
        let width = (collectionView.bounds.width) / numCellInRow
        return CGSize(width: width, height: 180)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.carModelsCollectionView {
            let endScrolling = (scrollView.contentOffset.y + scrollView.frame.size.height)
            
            if endScrolling >= scrollView.contentSize.height {
                if viewModel.checkIsHasNext() {
                    viewModel.getCarModelLApi()
                }
            }
        }
    }
}

