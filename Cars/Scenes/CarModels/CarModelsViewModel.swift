//
//  CarModelsViewModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 17/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol CarModelsViewModelProtocol {
    var carModelListSubject: BehaviorRelay<[CarModelsNames]> { get }
    func getCarModelLApi()
    func getCarPhotosApi(modelName: String, index: Int)
    func checkIsHasNext() -> Bool
    func navigateToDetailsVC(modelNiceName: String, modelName: String)
    var showProgress: PublishSubject<Bool> { get }
    var error: PublishSubject<Error> { get }
}

class CarModelsViewModel: CarModelsViewModelProtocol {
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private var carModelsApi: ApiClientProtocol
    let error = PublishSubject<Error>()
    let showProgress = PublishSubject<Bool>()
    private var carMakeName = ""
    private var totalPages: Int = 0
    private var pageNumber: Int = 1
    let carModelListSubject = BehaviorRelay<[CarModelsNames]>(value: [])
    private var carModelsList: [CarModelsNames] = [] {
        didSet {
            self.carModelListSubject.accept(carModelsList)
        }
    }
    
    // MARK: - Initialization
    init(carMakeName: String, carModelsApi: ApiClientProtocol = ApiClient()) {
        self.carModelsApi = carModelsApi
        self.carMakeName = carMakeName
    }
    
    // MARK: - Methods
    func checkIsHasNext() -> Bool {
        return (pageNumber > totalPages) ? false : true
    }
    
    func navigateToDetailsVC(modelNiceName: String, modelName: String) {
        try? AppNavigator().push(.detailsVC(carMake: self.carMakeName, modelNiceName: modelNiceName, modelName: modelName))
    }
    
    private func setCarImageSource(index: Int, imageSource: String) {
        self.carModelsList[index].imageSource = imageSource
    }
    
    func getCarModelLApi() {
        self.showProgress.onNext(true)
        self.carModelsApi.request(CarModelsAPI.models(carMakeName: carMakeName, pageNum: "\(pageNumber)", pageSize: "10", responseFields: Constants.MODELFIELDS), decodingType: CarModelsModel.self)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                self.carModelsList.append(contentsOf: response.results ?? [])
                self.totalPages = response.totalPages ?? 0
                self.pageNumber += 1
                self.showProgress.onNext(false)
            }, onError: { error in
                self.error.onNext(error)
                self.showProgress.onNext(false)
            }).disposed(by: disposeBag)
    }
    
    func getCarPhotosApi(modelName: String, index: Int) {
        self.carModelsApi.request(CarPhotosAPI.photos(carMakeName: self.carMakeName.replacingOccurrences(of: " ", with: ""), carModelName: modelName.replacingOccurrences(of: " ", with: ""), year: "\(Constants.YEAR)", category: Constants.EXTERIOR), decodingType: PhotosModel.self)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                if let imageSource = response.photos?.first?.sources?.first?.link?.href {
                    self.setCarImageSource(index: index, imageSource: imageSource)
                }
            }, onError: { error in
                print("Error", error.localizedDescription)
            }).disposed(by: disposeBag)
    }
}
