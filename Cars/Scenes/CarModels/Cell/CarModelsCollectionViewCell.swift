//
//  CarModelsCollectionViewCell.swift
//  Cars
//
//  Created by Eslam Mostafa on 17/02/2022.
//

import UIKit

class CarModelsCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var carModelImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - Properties
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    //MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCellUI()
    }
    
    //MARK: - Methods
    func setupCell(with dataModel: CarModelsNames) {
        carModelNameLabel.text = dataModel.name
        carModelImageView.loadFromUrl(stringUrl: dataModel.imageSource)
    }
    
    private func setupCellUI() {
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 16
        containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#F5F7FB")
    }
}
