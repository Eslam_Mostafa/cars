//
//  PhotosModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 18/02/2022.
//

import Foundation

struct PhotosModel: Codable {
    let photos: [Photos]?
    
    enum CodingKeys: String, CodingKey {
        case photos = "photos"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        photos = try values.decodeIfPresent([Photos].self, forKey: .photos)
    }
}

struct Photos: Codable {
    let title: String?
    let category: String?
    let sources: [Sources]?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case category = "category"
        case sources = "sources"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        sources = try values.decodeIfPresent([Sources].self, forKey: .sources)
    }
}

struct Sources: Codable {
    let link: Link?
    
    enum CodingKeys: String, CodingKey {
        case link = "link"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        link = try values.decodeIfPresent(Link.self, forKey: .link)
    }
}

struct Link: Codable {
    let rel: String?
    let href: String?
    
    enum CodingKeys: String, CodingKey {
        case rel = "rel"
        case href = "href"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        rel = try values.decodeIfPresent(String.self, forKey: .rel)
        href = try values.decodeIfPresent(String.self, forKey: .href)
    }
}
