//
//  CarPhotosViewController.swift
//  Cars
//
//  Created by Eslam Mostafa on 18/02/2022.
//

import UIKit
import ImageSlideshow
import RxSwift

class CarPhotosViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var dismissButton: UIButton!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: CarPhotosViewModelProtocol
    
    // MARK: - Initialization
    init(with viewModel: CarPhotosViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBinding()
        viewModel.getCarPhotosApi()
    }
    
    // MARK: - Methods
    private func configureBinding() {
        viewModel.photosSubject.skip(1)
            .subscribe(onNext: { [weak self] carPhotos in
                guard let self = self else { return }
                self.configureSlideShow(list: carPhotos)
            }).disposed(by: disposeBag)
        
        dismissButton.rx.tap
            .subscribe({ [weak self] _ in
                guard let self = self else { return }
                self.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    
    private func configureSlideShow(list : [String]) {
        imageSlider.layer.cornerRadius = 10
        imageSlider.slideshowInterval = 5.0
        imageSlider.contentScaleMode = UIViewContentMode.scaleToFill
        imageSlider.contentMode = .scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.pageIndicatorTintColor = PublicMethods.hexaStringToUIColor("#d2d2d2")
        imageSlider.pageIndicator = pageControl
        imageSlider.activityIndicator = DefaultActivityIndicator()
        
        var slideArray = [InputSource]()
        
        if list.count > 0 {
            for item in list {
                if let url = URL(string: Constants.IMAGESOURCE + item) {
                    print("ImageUrl: - ",url)
                    slideArray.append(SDWebImageSource(url: url, placeholder: UIImage(named: "Logo") ?? #imageLiteral(resourceName: "slider")))
                } else {
                    slideArray.append(ImageSource(image: UIImage(named: "Logo") ?? #imageLiteral(resourceName: "Logo")))
                }
            }
        } else {
            slideArray.append(ImageSource(image: UIImage(named: "Logo") ?? #imageLiteral(resourceName: "Logo")))
        }
        
        self.imageSlider.setImageInputs(slideArray)
    }
}
