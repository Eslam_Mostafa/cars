//
//  CarPhotosViewModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 18/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol CarPhotosViewModelProtocol {
    var photosSubject: BehaviorRelay<[String]> { get }
    func getCarPhotosApi()
}

class CarPhotosViewModel: CarPhotosViewModelProtocol {
    
    // MARK: - Properties
    private var carMake = ""
    private var carModel = ""
    private var category = ""
    private let disposeBag = DisposeBag()
    private var carPhotosApi : ApiClientProtocol
    var photosSubject = BehaviorRelay<[String]>(value: [])
    
    // MARK: - Initialization
    init(carMake: String, carModel: String, category: String, carPhotosApi: ApiClientProtocol = ApiClient()) {
        self.carMake = carMake
        self.carModel = carModel
        self.category = category
        self.carPhotosApi = carPhotosApi
    }
    
    // MARK: - Methods
    func getCarPhotosApi() {
        self.carPhotosApi.request(CarPhotosAPI.photos(carMakeName: self.carMake.replacingOccurrences(of: " ", with: ""), carModelName: carModel.replacingOccurrences(of: " ", with: ""), year: "\(Constants.YEAR)", category: category), decodingType: PhotosModel.self)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                if let photos = response.photos {
                    let sources = photos.map({$0.sources ?? []}).joined()
                    self.photosSubject.accept(sources.map({ $0.link?.href ?? "" }))
                }
            }, onError: { error in
                
            }).disposed(by: disposeBag)
    }
}
