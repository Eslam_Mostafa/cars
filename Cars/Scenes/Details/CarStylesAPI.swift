//
//  CarStylesAPI.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import Foundation
import Alamofire

enum CarStylesAPI: URLRequestConvertible {
    case styles(carMakeName: String, modelNiceName: String, pageNum: String, pageSize: String, responseFields: String)
}

extension CarStylesAPI: ApiRouter {
    var path: String {
        switch self {
        case .styles:
            return "styles"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .styles:
            return .get
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .styles(let carMakeName, let modelNiceName, let pageNum , let pageSize, let responseFields):
            return [
                Constants.Parameters.MAKENAME : carMakeName,
                Constants.Parameters.MODELNICENAME : modelNiceName,
                Constants.Parameters.APIKEY : Constants.APIKEY,
                Constants.Parameters.YEAR : Constants.YEAR,
                Constants.Parameters.PAGE_NUMBER : pageNum,
                Constants.Parameters.PAGE_SIZE : pageSize,
                Constants.Parameters.RESPONSE_FIELDS : responseFields
            ]
        }
    }
}
