//
//  DetailCollectionViewCell.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Properties
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    // MARK: - Methods
    func setupCell(with dataModel: String, backGroundColor: String) {
        detailLabel.text = dataModel
        containerView.backgroundColor = PublicMethods.hexaStringToUIColor(backGroundColor)
    }
}
