//
//  DetailsCellModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 20/02/2022.
//

import Foundation
import RxCocoa

struct DetailsCellModel {
    var bgColor = ""
    var imageName = ""
    var cellTitle = ""
    var collectionDataListSubject = BehaviorRelay<[String]>(value: [])
}
