//
//  DetailsCellViewModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol DetailsCellViewModelProtocol {
    func getTableCellModel() -> DetailsCellModel
    func getCollectionCellAt(index: Int) -> String
}

class DetailsCellViewModel: DetailsCellViewModelProtocol {
    
    // MARK: - Properties
    private var cellDataModel: DetailsCellModel
    private var collectionDataList : [String] = []
    
    // MARK: - Initialization
    init(with dataModel: DetailsCellModel) {
        self.cellDataModel = dataModel
        self.collectionDataList = dataModel.collectionDataListSubject.value
    }
    
    // MARK: - Methods
    func getTableCellModel() -> DetailsCellModel {
        return self.cellDataModel
    }
    
    func getCollectionCellAt(index: Int) -> String {
        return collectionDataList[index]
    }
}
