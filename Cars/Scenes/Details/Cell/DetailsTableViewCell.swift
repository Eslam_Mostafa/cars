//
//  DetailsTableViewCell.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class DetailsTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout {
    
    //MARK: - Outlets
    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var detailCollectionView: UICollectionView!
    @IBOutlet weak var detailImageView: UIImageView!
    
    //MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupDetailCollectionView()
    }
    
    //MARK: - Properties
    private let disposeBag = DisposeBag()
    private var viewModel: DetailsCellViewModelProtocol!
    
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    func setupCell(viewModel: DetailsCellViewModelProtocol) {
        self.viewModel = viewModel
        let cellModel = viewModel.getTableCellModel()
        detailTitleLabel.text = cellModel.cellTitle
        detailImageView.image = UIImage(named: cellModel.imageName)
        connfigureBinding(dataModel: cellModel)
    }
    
    private func setupDetailCollectionView() {
        detailCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        detailCollectionView.register(DetailCollectionViewCell.nib, forCellWithReuseIdentifier: DetailCollectionViewCell.identifier)
    }
    
    private func connfigureBinding(dataModel: DetailsCellModel) {
        dataModel.collectionDataListSubject.bind(to: detailCollectionView.rx.items(cellIdentifier: DetailCollectionViewCell.identifier, cellType: DetailCollectionViewCell.self)) { index, carMakesModel, cell in
            cell.setupCell(with: carMakesModel, backGroundColor: dataModel.bgColor)
        }.disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        label.text = self.viewModel.getCollectionCellAt(index: indexPath.item)
        label.sizeToFit()
        let width = label.frame.width + 20
        
        return CGSize(width: width, height: 32)
    }
}
