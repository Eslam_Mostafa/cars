//
//  DetailsModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import Foundation

struct DetailsModel: Codable {
    let totalNumber: Int?
    let totalPages: Int?
    let results: [CarStyles]?
    
    enum CodingKeys: String, CodingKey {
        case totalNumber = "totalNumber"
        case totalPages = "totalPages"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        totalNumber = try values.decodeIfPresent(Int.self, forKey: .totalNumber)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        results = try values.decodeIfPresent([CarStyles].self, forKey: .results)
    }
}

struct CarStyles: Codable {
    let transmissionType: String?
    let engineType: String?
    let engineSize: Double?
    let numberOfSeats: Int?
    let categories: Categories?
    
    enum CodingKeys: String, CodingKey {
        case transmissionType = "transmissionType"
        case engineType = "engineType"
        case engineSize = "engineSize"
        case numberOfSeats = "numberOfSeats"
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transmissionType = try values.decodeIfPresent(String.self, forKey: .transmissionType)
        engineType = try values.decodeIfPresent(String.self, forKey: .engineType)
        engineSize = try values.decodeIfPresent(Double.self, forKey: .engineSize)
        numberOfSeats = try values.decodeIfPresent(Int.self, forKey: .numberOfSeats)
        categories = try values.decodeIfPresent(Categories.self, forKey: .categories)
    }
}

struct Categories: Codable {
    let pRIMARY_BODY_TYPE: [String]?
    let vehicleType: [String]?
    let vehicleStyle: [String]?
    let market: [String]?
    let vehicleSize: [String]?
    
    enum CodingKeys: String, CodingKey {
        case pRIMARY_BODY_TYPE = "PRIMARY_BODY_TYPE"
        case vehicleType = "Vehicle Type"
        case vehicleStyle = "Vehicle Style"
        case market = "Market"
        case vehicleSize = "Vehicle Size"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pRIMARY_BODY_TYPE = try values.decodeIfPresent([String].self, forKey: .pRIMARY_BODY_TYPE)
        vehicleType = try values.decodeIfPresent([String].self, forKey: .vehicleType)
        vehicleStyle = try values.decodeIfPresent([String].self, forKey: .vehicleStyle)
        market = try values.decodeIfPresent([String].self, forKey: .market)
        vehicleSize = try values.decodeIfPresent([String].self, forKey: .vehicleSize)
    }
}
