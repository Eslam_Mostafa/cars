//
//  DetailsViewController.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class DetailsViewController: UIViewController, Loadable, UIScrollViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var exteriorButton: UIButton!
    @IBOutlet weak var interiorButton: UIButton!
    @IBOutlet weak var detailsTableView: UITableView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: DetailsViewModelProtocol
    
    // MARK: - Initialization
    init(with viewModel: DetailsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUIText()
        setupDetailsTableView()
        configureBinding()
        viewModel.getCarStylesApi()
    }
    
    // MARK: - Methods
    private func setUIText() {
        self.title = "Model details"
    }
    
    private func setupDetailsTableView() {
        detailsTableView.rx.setDelegate(self).disposed(by: disposeBag)
        detailsTableView.register(DetailsTableViewCell.nib, forCellReuseIdentifier: DetailsTableViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.showProgress
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self]  in
                guard let self = self else { return }
                self.showLoading(show: $0)
            }).disposed(by: disposeBag)
        
        viewModel.error
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(alertTitle: "Something wrong!", alertMessge: error.localizedDescription, actionTitle: "Cancel")
            }).disposed(by: disposeBag)
        
        viewModel.modelNameSubject
            .bind(to: self.modelNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.detailsSubject.bind(to: detailsTableView.rx.items(cellIdentifier: DetailsTableViewCell.identifier, cellType: DetailsTableViewCell.self)) { index, cellModel, cell in
            let cellViewModel = DetailsCellViewModel(with: cellModel)
            cell.setupCell(viewModel: cellViewModel)
        }.disposed(by: disposeBag)
        
        exteriorButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.viewModel.navigateToPhotosVC(category: Constants.EXTERIOR)
            }).disposed(by: disposeBag)
        
        interiorButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.viewModel.navigateToPhotosVC(category: Constants.INTERIOR)
            }).disposed(by: disposeBag)
    }
}
