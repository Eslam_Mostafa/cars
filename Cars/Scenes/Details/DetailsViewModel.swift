//
//  DetailsViewModel.swift
//  Cars
//
//  Created by Eslam Mostafa on 19/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol DetailsViewModelProtocol {
    var modelNameSubject: BehaviorRelay<String>{ get }
    var showProgress: PublishSubject<Bool> { get }
    var error: PublishSubject<Error> { get }
    var detailsSubject: BehaviorRelay<[DetailsCellModel]>{ get }
    func getCarStylesApi()
    func navigateToPhotosVC(category: String)
}

class DetailsViewModel: DetailsViewModelProtocol {
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private var carStylesApi: ApiClientProtocol
    let modelNameSubject = BehaviorRelay<String>(value: "")
    let error = PublishSubject<Error>()
    let showProgress = PublishSubject<Bool>()
    let detailsSubject = BehaviorRelay<[DetailsCellModel]>(value: [])
    private var makeName = ""
    private var modelNiceName = ""
    private var modelName = ""
    private var detailList: [DetailsCellModel] = []
    
    // MARK: - Initialization
    init(makeName: String, modelNiceName: String, modelName: String, carStylesApi: ApiClientProtocol = ApiClient()) {
        self.carStylesApi = carStylesApi
        self.makeName = makeName
        self.modelName = modelName
        self.modelNiceName = modelNiceName
        self.modelNameSubject.accept(modelName)
    }
    
    // MARK: - Methods
    private func setupEngineList(styles: [CarStyles]) {
        let engineList = styles.map({"\($0.engineType ?? "") / \($0.engineSize ?? 0)"})
        detailList.append(DetailsCellModel(bgColor: "#243454", imageName: "ic_engine", cellTitle: "Engine type / size", collectionDataListSubject: BehaviorRelay<[String]>(value: Array(Set(engineList)))))
    }
    
    private func setupTransmissionTypeList(styles: [CarStyles]) {
        let transmissionTypeList = styles.map({"\($0.transmissionType ?? "")"})
        detailList.append(DetailsCellModel(bgColor: "#000000", imageName: "transmission", cellTitle: "Transmission type", collectionDataListSubject: BehaviorRelay<[String]>(value: Array(Set(transmissionTypeList)))))
    }
    
    private func setupVehicleStyleAndSizeList(styles: [CarStyles]) {
        let vehicleStyleAndSizeList = styles.map({"\($0.categories?.vehicleStyle?.first ?? "") - \($0.categories?.vehicleSize?.first ?? "") / \($0.numberOfSeats ?? 0)"})
        detailList.append(DetailsCellModel(bgColor: "#842432", imageName: "car", cellTitle: "Vehicle style - size / number of seats", collectionDataListSubject: BehaviorRelay<[String]>(value: Array(Set(vehicleStyleAndSizeList)))))
    }
    
    private func setupDetailsData(styles: [CarStyles]) {
        self.setupEngineList(styles: styles)
        self.setupTransmissionTypeList(styles: styles)
        self.setupVehicleStyleAndSizeList(styles: styles)
        self.detailsSubject.accept(self.detailList)
    }
    
    func navigateToPhotosVC(category: String) {
        try? AppNavigator().present(.carPhotosVC(carMake: self.makeName, carModel: self.modelName, category: category))
    }
    
    func getCarStylesApi() {
        self.showProgress.onNext(true)
        self.carStylesApi.request(CarStylesAPI.styles(carMakeName: self.makeName.replacingOccurrences(of: " ", with: ""), modelNiceName: self.modelNiceName.replacingOccurrences(of: " ", with: ""), pageNum: "1", pageSize: "30", responseFields: Constants.STYLESFIELDS), decodingType: DetailsModel.self)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { response in
                self.setupDetailsData(styles: response.results ?? [])
                self.showProgress.onNext(false)
            }, onError: { error in
                self.error.onNext(error)
                self.showProgress.onNext(false)
            }).disposed(by: disposeBag)
    }
}
